package Clases;

import com.sun.speech.freetts.*;

public class Speech {
    /**
     * Clase encargada de pasar texto a voz
     */
    private static final String Voicename = "kevin16";
    /**
     * @param VOZ
     */
    public void Play(String VOZ){
        Voice voice;
        VoiceManager vm = VoiceManager.getInstance();
        voice=vm.getVoice(Voicename);
        
        voice.allocate();
        
        try {
            voice.speak(VOZ);
        } catch (Exception e) {
            System.out.println("Error: " + e);
        }
        
    }
    
}
