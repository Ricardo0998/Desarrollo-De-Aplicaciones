package Clases;

import java.io.FileWriter;
import java.io.IOException;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import org.json.simple.JSONObject;
/**
 * Metod encargado de guardar los datos de conexion para el buen funcionamiento de la app
 */
public class SaveJson { 
    String Guardar;
    
        public String SaveJson(JTextField TF_Usu, JTextField TF_Password, JTextField TF_BD, JTextField TF_Ruta) {
        JSONObject obj = new JSONObject();

        String URL = "jdbc:mysql://" + TF_Ruta.getText() + "/";

        obj.put("url", URL);
        obj.put("bd", TF_BD.getText());
        obj.put("usu", TF_Usu.getText());
        obj.put("pass", TF_Password.getText());

        try {

            try (FileWriter file = new FileWriter("Z:\\Saves\\Save.json")) {
                file.write(obj.toJSONString());
                file.flush();
            }

            Guardar = "Was properly saved";

        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "Error " + e);
        }

        System.out.print(obj);
        return Guardar;
    }
}