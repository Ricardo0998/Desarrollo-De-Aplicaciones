
package sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;


public class Conn {


    Connection c;
    
    String server;
    String user;
    String pass;
    String db;
    
    Statement st;
    
    public Conn() {
        
        try{
        
            Class.forName("");
            
            c = DriverManager.getConnection("", user, pass);
            
            if(c != null)
                st = c.createStatement();
            
        }
        catch(Exception e){
            
            System.out.println("Error: " + e.getMessage());
        }
    }
    
    
    public ResultSet query(String strQuery){
        
        try{
            if(st != null)
                return st.executeQuery(strQuery);            
        }
        catch(Exception e){
            System.out.println("Error: " + e.getMessage());
        }
        
        return null;
    }
    
    public void queryExce(String strQuery){
        
        try{
            if(st != null)
                st.execute(strQuery);            
        }
        catch(Exception e){
            System.out.println("Error: " + e.getMessage());
        }
    }
    
}
