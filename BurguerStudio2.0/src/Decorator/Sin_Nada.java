package Decorator;

public class Sin_Nada extends DecoradorHamburguesa{
	private Hamburguesa hamburguesa;

	public Sin_Nada(Hamburguesa h){
		this.hamburguesa = h;
	}

	public String getDescripcion(){
		return hamburguesa.getDescripcion();
	}
}