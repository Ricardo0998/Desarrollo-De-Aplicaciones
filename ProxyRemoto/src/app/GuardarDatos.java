
package app;



public class GuardarDatos implements IGuardar{

    @Override
    public void save(String datosAGuardar) {

        if(ConnectionManager.hayConecxion()){
            
            new ObjetoRemoto().save(datosAGuardar);
            new GuardarEnCorreo().save(datosAGuardar);
        }
        
        else
            new GuargarDiscoDuro().save(datosAGuardar);
    }
    
}