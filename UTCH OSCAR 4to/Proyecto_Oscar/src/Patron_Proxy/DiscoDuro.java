package Patron_Proxy;

public class DiscoDuro implements Guardar {

    public void save(String datosAGuardar, double precio) { //Se manda a disco duro si no existe conexion
        
        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
        
        System.out.println("Ingredientes: " + datosAGuardar);
        System.out.println("Precio total: " + precio);
        System.out.println("Esperando conexión a internet");
    }

}