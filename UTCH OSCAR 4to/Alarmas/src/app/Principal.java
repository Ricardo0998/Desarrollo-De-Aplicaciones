
package app;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;


public class Principal {


    boolean activo = false;
    
    public HashMap<String, ArrayList<Alarma>> listaAlarmas = new HashMap();
    Thread hilo;
    
    public void addAlarma(String t, Alarma a){
        
        ArrayList arr = listaAlarmas.get(t);
        
        if(arr == null)                   
            arr = new ArrayList();
            
        arr.add(a);
        listaAlarmas.put(t, arr);
    }
    
    public void upDateAlarma(String t, Alarma a){
    
        //listaAlarmas.put(t, a);
    }
    
    public void deleteAlarma(String t){
    
        listaAlarmas.remove(t);
    }
    
    public void stopHilo(){
    
        activo = false;
    }
    
    public void startHilo(){
    
        System.out.println("Antes hilo");
        if(!activo){
            
            activo = true;
            System.out.println("Hilo");
            hilo = new Thread(
                    new Runnable() {
                        @Override
                        public void run() {

                            while(activo){

                                try {

                                    Thread.sleep(1 * 1000);
                                    //Mon Oct 30 07:46:41 MST 2017

                                    String e [] = new Date().toString().split(" ");

                                    String cade = e[1]+e[2]+e[3];
                                    ArrayList<Alarma> a = listaAlarmas.get(cade);

                                    if(a != null)
                                        for (Alarma al : a) 
                                            new Thread(al).start();
                                        
                                        

                                } catch (Exception e) {
                                    System.out.println("Error: " + e.getMessage());
                                }
                            }
                        }
                    }
            );

            hilo.start();
        }
    }
    
}
