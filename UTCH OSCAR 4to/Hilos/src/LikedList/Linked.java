package LikedList;

import app.node;

public class Linked<T> {

    private node<T> sentinel = null;

    public Linked() {
        sentinel = new node<T>();
        sentinel.setIndex(-1);

    }

    public Linked(String hora, String minuto, String ampm, String link) {
        this();
        node<T> tmp = new node<T>(hora, minuto, ampm, link);
        tmp.setIndex(0);
        sentinel.setNext(tmp);

    }

    public void add(String hora, String minuto, String ampm, String link) { //AGREGAR
        node<T> tmp = sentinel;

        while (tmp.getNext() != null) {
            tmp = tmp.getNext();
        }
        tmp.setNext(new node<T>(hora, minuto, ampm, link));

    }
}
